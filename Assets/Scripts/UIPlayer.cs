﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventManagerUnity;
using EventManagerUnity.Utils;

namespace EventManagerUnity
{
    public class UIPlayer : MonoBehaviour
    {
        public void Load()
        {

        }

        public void EventTester()
        {
            Debug.Log("INSIDE UIPlayer.EventTester()");

            Definitions.EventParam eventParams = new Definitions.EventParam();
            eventParams.param1 = "foo-string";
            //eventParams.param2 = 80085;
            eventParams.param3 = 133.7f;
            //eventParams.param4 = false;
            EventManager.TriggerEvent(Definitions.EventTesterCallback, eventParams);
        }

    }

}