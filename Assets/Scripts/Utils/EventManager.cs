﻿using UnityEngine;
using UnityEngine.Events;
using System; //for using "Action"
using System.Collections;
using System.Collections.Generic;
using EventManagerUnity.Utils;

//REFERENCE: https://stackoverflow.com/questions/42034245/unity-eventmanager-with-delegate-instead-of-unityevent
namespace EventManagerUnity.Utils
{

    public class EventManager : MonoBehaviour
    {

        private Dictionary<string, Action<Definitions.EventParam>> eventDictionary;

        private static EventManager eventManager;

        public static EventManager instance
        {
            get
            {
                if (!eventManager)
                {
                    eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                    if (!eventManager)
                    {
                        Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                    }
                    else
                    {
                        eventManager.Init();
                    }
                }

                return eventManager;
            }
        }

        void Init()
        {
            if (eventDictionary == null)
            {
                eventDictionary = new Dictionary<string, Action<Definitions.EventParam>>();
            }
        }

        public static void StartListening(string eventName, Action<Definitions.EventParam> listener)
        {
            Action<Definitions.EventParam> thisEvent;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                //Add more event to the existing one
                thisEvent += listener;

                //Update the Dictionary
                instance.eventDictionary[eventName] = thisEvent;
            }
            else
            {
                //Add event to the Dictionary for the first time
                thisEvent += listener;
                instance.eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(string eventName, Action<Definitions.EventParam> listener)
        {
            if (eventManager == null) return;
            Action<Definitions.EventParam> thisEvent;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                //Remove event from the existing one
                thisEvent -= listener;

                //Update the Dictionary
                instance.eventDictionary[eventName] = thisEvent;
            }
        }

        public static void TriggerEvent(string eventName, Definitions.EventParam eventParam)
        {
            Action<Definitions.EventParam> thisEvent = null;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke(eventParam);
                // OR USE  instance.eventDictionary[eventName](eventParam);
            }
        }

    }
}