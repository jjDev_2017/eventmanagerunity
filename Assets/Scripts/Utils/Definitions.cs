﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EventManagerUnity.Utils
{
    public class Definitions
    {
        //Re-usable structure/ Can be a class to. Add all parameters you need inside it
        public struct EventParam
        {
            public string param1;
            public int param2;
            public float param3;
            public bool param4;
        }


        public const string EventTesterCallback = "EventTesterCallback";

    }
}