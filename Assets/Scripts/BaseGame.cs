﻿using System; //For using "Action"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventManagerUnity;
using EventManagerUnity.Utils;

namespace EventManagerUnity
{
    public class BaseGame : MonoBehaviour
    {

        public UIPlayer playerA;

        private Action<Definitions.EventParam> TesterListener;

        void Start()
        {
            Debug.Log("BaseGame started " + Time.realtimeSinceStartup);
            TesterListener = new Action<Definitions.EventParam>(TestCallback);
            EventManager.StartListening(Definitions.EventTesterCallback, TesterListener);

            playerA.EventTester();
        }


        private void TestCallback(Definitions.EventParam eventParam)
        {
            Debug.Log("Entered TestCallback " + eventParam.param1);
        }

    }
}